module Components.Hand where

import Components.Card as Card


-- MODEL

type alias Model =
  { cards : List Card.Model
  }

type Sum
  = BlackjackOrLower Int
  | Busted

sum : Model -> Sum
sum model =
  let
    sumsUpToBlackjack =
      List.filter (\x -> x <= 21) allPossibleSums
      |> List.sort
      |> List.reverse

    allPossibleSums =
      List.foldl addCard [0] model.cards

    addCard : Card.Model -> List Int -> List Int
    addCard card currentSums =
      let
        incrementCurrentSums value =
          List.map ((+) value) currentSums
      in
        List.map incrementCurrentSums card.numericValues
        |> List.concat

  in
    case sumsUpToBlackjack of
      highestSum :: _ ->
        BlackjackOrLower highestSum

      _ ->
        Busted


-- UPDATE

addCard : Card.Model -> Model -> Model
addCard card model =
  { cards = List.append model.cards [card]
  }
