module Components.Deck where

import Random exposing
  ( Seed
  )
import Components.Card as Card exposing
  ( Rank(Numeric, Face, Ace)
  , FaceType(Jack, Queen, King)
  , Suit(Clubs, Diamonds, Hearts, Spades)
  )
import Components.Hand as Hand exposing
  ( Sum(BlackjackOrLower, Busted)
  )


-- MODEL

type alias Model =
  { remainingCards : List Card.Model
  , nextSeed : Seed
  }

init : Seed -> Model
init seed =
  let
    fullDeck =
      List.concatMap suitCards [Clubs, Diamonds, Hearts, Spades]

    suitCards suit =
      List.map (Card.init suit) suitRanks

    suitRanks =
      List.map Numeric [2..10]
      ++ List.map Face [Jack, Queen, King]
      ++ [Ace]
  in
    { remainingCards = fullDeck
    , nextSeed = seed
    }


-- UPDATE

dealCard : Model -> (Maybe Card.Model, Model)
dealCard model =
  let
    cardsInDeck =
      List.length model.remainingCards

    remainingCards =
      List.take nextCardId model.remainingCards
      ++ cardsAfterDealt

    (nextCardId, nextSeed) =
      Random.generate
        (Random.int 0 <| cardsInDeck - 1)
        model.nextSeed

    (dealtCard, cardsAfterDealt) =
      case List.drop nextCardId model.remainingCards of
        dealtCard :: cardsAfterDealt ->
          (Just dealtCard, cardsAfterDealt)

        _ ->
          (Nothing, [])

  in
    ( dealtCard
    , { remainingCards = remainingCards
      , nextSeed = nextSeed
      }
    )

hitRestToDealer : Hand.Model -> Model -> (Hand.Model, Model)
hitRestToDealer initialDealerHand model =
  let
    dealerHand currentHand currentDeck =
      case Hand.sum currentHand of
        Busted ->
          (currentHand, currentDeck)

        BlackjackOrLower sum ->
          if (sum >= 17)
            then (currentHand, currentDeck)
            else
              let (nextCard, nextDeck) = dealCard currentDeck
              in
                case nextCard of
                  Just card ->
                    dealerHand (Hand.addCard card currentHand) nextDeck

                  Nothing ->
                    -- This should never happen.
                    Debug.crash "Not enough cards in the deck! 😵"

  in
    dealerHand initialDealerHand model
