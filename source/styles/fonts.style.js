module.exports = [{
  '@font-face': {
    'font-family': '"Source Sans Pro"',
    'src': 'url("/fonts/source-sans-pro.regular.woff") format("woff")',
  },
}, {
  '@font-face': {
    'font-family': '"Source Sans Pro"',
    'font-weight': '900',
    'src': 'url("/fonts/source-sans-pro.black.woff") format("woff")',
  },
}];
