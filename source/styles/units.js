const g = require('./global');

const px = exports.px = (pixels) => (
  `${pixels / g.browserDefaultFontSize}rem`
);

// Source Code Pro is about 1.3 smaller than Roboto. We have to increase
// every font size by this factor.
exports.sp = (scalablePixels) => (
  px(scalablePixels * 1.3)
);
