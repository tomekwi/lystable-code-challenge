const materialColors = require('material-colors');

exports.primary = {
  light: materialColors.grey[200],
  base: materialColors.grey[600],
  dark: materialColors.grey[800],
};

exports.accent =
  materialColors.red[900];
