# Blackjack

**A code challenge over at Lystable**



## Getting started

```sh
npm install
scripts/start
# Navigate to http://localhost:9911
```



## Notes

* The initial seed for pseudo-random numbers is constant. It means that every time you hit buttons in a certain order, you get the same results. This is convenient for testing, but in a real-world app we’d rather take the seed from a true random number generator like https://www.random.org/clients/http/.
* No polyfills inside! Unlikely to work 100% outside Chrome Evergreen.
* `Hand` is a pure logic component. Now that the UI grew a bit, I’d refector it into a full-fledged component if I had more time. But I decided to leave it to show what pure-logic components look like.
