/* eslint-disable max-len */

// See
// https://www.google.com/design/spec/style/color.html#color-text-background-colors

const u = require('./utils');

exports.primary =
  u.blackOpacity(0.87);

exports.secondary =
  u.blackOpacity(0.54);

exports.hint =
  u.blackOpacity(0.38);

exports.white =
  'white';
