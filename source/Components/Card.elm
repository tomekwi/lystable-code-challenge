module Components.Card where

import Html exposing (Html, div, text)
import Html.Attributes exposing (class, title)
import String


-- MODEL

type alias Model =
  { rank : Rank
  , numericValues : List Int
  , suit : Suit
  }

type Rank
  = Numeric Int
  | Face FaceType
  | Ace

type FaceType
  = Jack
  | Queen
  | King

type Suit
  = Clubs
  | Diamonds
  | Hearts
  | Spades

init : Suit -> Rank -> Model
init suit rank =
  { rank = rank
  , suit = suit
  , numericValues = case rank of
    Numeric numericalValue -> [numericalValue]
    Face _ -> [10]
    Ace -> [1, 11]
  }


-- VIEW

view : Model -> Html
view model =
  let
    rankSymbol =
      case model.rank of
        Numeric value -> toString value
        Face Jack -> "J"
        Face Queen -> "Q"
        Face King -> "K"
        Ace -> "A"

    suitFlag =
      case model.suit of
        Clubs -> "clubs"
        Diamonds -> "diamonds"
        Hearts -> "hearts"
        Spades -> "spades"

    suitSymbol =
       case model.suit of
        Clubs -> "♣"
        Diamonds -> "♦"
        Hearts -> "♥"
        Spades -> "♠"

  in
    div
      [ class <| String.join " "
        [ "card"
        , "card·" ++ suitFlag
        ]
      , title <| suitSymbol ++ rankSymbol
      ]
      [ text rankSymbol
      ]
