const minViewportWidth =
  450;

const minViewportHeight =
  530;

module.exports = {
  [`@media screen and (max-width: ${minViewportWidth}px)`]: {
    'html': {
      'font-size': `${16 / minViewportWidth * 100}vw`,
    },
  },

  [`@media screen and (max-height: ${minViewportHeight}px)`]: {
    'html': {
      'font-size': `${16 / minViewportHeight * 100}vh`,
    },
  },

  '@-ms-viewport': {
    'width': 'device-width',
  },

  'html, body, #main': {
    'width': '100%',
    'height': '100%',
  },

  '*, *::before, *::after': {
    'margin': '0',
    'padding': '0',
    'box-sizing': 'border-box',
    'font': 'inherit',
    'line-height': 'inherit',
  },
};
