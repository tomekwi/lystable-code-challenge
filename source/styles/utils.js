const repeat = require('repeat-string');

exports.blackOpacity = (factor) => (
  `rgba(0, 0, 0, ${factor})`
);

exports.raiseSpecificity = (factor, baseSelector) => (
  baseSelector + repeat('[class]', factor)
);
