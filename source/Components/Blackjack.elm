module Components.Blackjack
  ( Model, Action
  , init, update, view
  ) where

import String
import Html exposing (Html, div, h2, text)
import Html.Attributes exposing (class)
import Random exposing (Seed)
import Components.Hand as Hand exposing
  ( Sum(BlackjackOrLower, Busted)
  )
import Components.Deck as Deck
import Components.Card as Card
import Components.Dashboard as Dashboard exposing
  ( GameStatus(Won, Draw, Lost, StillPlaying)
  )


-- MODEL

type alias Model =
  { deck : Deck.Model
  , dealerHand : Hand.Model
  , playerHand : Hand.Model
  , playerHasSaidStick : Bool
  }

init : Seed -> Model
init seed =
  let
    (initialDealerCard, deck) =
      getCard deckAfterDealingSecondCard

    (secondPlayerCard, deckAfterDealingSecondCard) =
      getCard deckAfterDealingFirstCard

    (firstPlayerCard, deckAfterDealingFirstCard) =
      getCard initialDeck

    initialDeck =
      Deck.init seed
  in
    { deck = deck
    , dealerHand = Hand.Model [initialDealerCard]
    , playerHand = Hand.Model [firstPlayerCard, secondPlayerCard]
    , playerHasSaidStick = False
    }

-- Private
getCard : Deck.Model -> (Card.Model, Deck.Model)
getCard deckModel =
  case Deck.dealCard deckModel of
    (Just card, deck) ->
      (card, deck)

    _ ->
      -- This should never happen.
      Debug.crash "Not enough cards in the deck! 😵"


-- UPDATE

type Action
  = Hit
  | Stick
  | Restart

update : Action -> Model -> Model
update action model =
  case action of
    Hit ->
      let
        (newCard, deck) =
          getCard model.deck
      in
        { model
        | playerHand = Hand.addCard newCard model.playerHand
        , deck = deck
        }

    Stick ->
      let
        (dealerHand, deck) =
          Deck.hitRestToDealer model.dealerHand model.deck
      in
        { model
        | dealerHand = dealerHand
        , deck = deck
        , playerHasSaidStick = True
        }

    Restart ->
      init model.deck.nextSeed


-- VIEW

view : Signal.Address Action -> Model -> Html
view address model =
  let
    context = Dashboard.Context
      (Signal.forwardTo address (always Hit))
      (Signal.forwardTo address (always Stick))
      (Signal.forwardTo address (always Restart))

    dashboardModel =
      { playerSum = playerSum
      , gameStatus = gameStatus
      }

    playerSum =
      Hand.sum model.playerHand

    gameStatus =
      let
        dealerSum =
          Hand.sum model.dealerHand

        playerSum =
          Hand.sum model.playerHand

      in
        case (dealerSum, playerSum, model.playerHasSaidStick) of
          (Busted, _, _) ->
            Won

          (_, Busted, _) ->
            Lost

          (BlackjackOrLower dealerScore, BlackjackOrLower playerScore, True) ->
            if (playerScore > dealerScore)
              then Won
            else if (playerScore == dealerScore)
              then Draw
              else Lost

          _ ->
            StillPlaying

    handView {playerName, label, cards} =
      div [classes
        [ "blackjack’s-hand-background"
        , "blackjack’s-hand-background·" ++ playerName
        ]]
        [ h2
          [ class "blackjack’s-hand-label"
          ]
          [ text label
          ]
        , div [classes
          [ "blackjack’s-hand"
          , "card’s-container"
          ]]
          <| List.map Card.view cards
        ]

    classes classList =
      classList
      |> String.join " "
      |> class

  in
    div
      [ class "blackjack"
      ]
      [ handView
        { playerName = "dealer"
        , label = "Dealer’s cards"
        , cards = model.dealerHand.cards
        }
      , handView
        { playerName = "player"
        , label = "Your cards"
        , cards = model.playerHand.cards
        }
      , Dashboard.view context dashboardModel
      ]
