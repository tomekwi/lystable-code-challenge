const merge = require('object-merge');

const colors = require('../styles/colors');
const textColors = require('../styles/textColors');
const utils = require('../styles/utils');
const units = require('../styles/units');

const dashboardItem = {
  'padding': units.px(20),
};

const shadowSpill = 20;

module.exports = {
  '.dashboard': {
    'display': 'flex',
    'overflow-x': 'auto',
    'background-color': colors.primary.base,
    'color': textColors.white,
    'box-shadow': [
      '0',
      units.px(shadowSpill + 2),
      units.px(shadowSpill),
      units.px(shadowSpill),
      utils.blackOpacity(0.6),
    ].join(' '),
  },

  '.dashboard’s-button': merge((
    dashboardItem
  ), {
    'flex-grow': '1',
    'text-align': 'center',
    'position': 'relative',
    'text-transform': 'uppercase',
    'border-radius': '0',
    'margin': '0',
  }),

  '.dashboard’s-button·primary': {
    'background-color': colors.primary.dark,
  },

  '.dashboard’s-status-bar': merge((
    dashboardItem
  ), {
    'flex-grow': '3',
    'font-size': units.sp(20),
  }),

  '.dashboard’s-button-icon': {
    'margin-top': '-0.15em',
  },
};
