import StartApp.Simple as StartApp
import Html exposing
  ( Html
  )
import Random exposing
  ( Seed
  )
import Components.Blackjack as Blackjack exposing
  ( init, update, view
  )

main : Signal Html
main =
  StartApp.start
    { model = init <| Random.initialSeed 0
    , update = update
    , view = view
    }
