const range = require('range').range;
const color = require('color');

const colors = require('../styles/colors');
const units = require('../styles/units');
const utils = require('../styles/utils');

const pxPerInch =
  40;
const elevation =
  2;

const cardHeight =
  pxPerInch * 3.5;
const cardWidth =
  pxPerInch * 2.5;
const cardSpacing =
  10;

const animationDuration =
  '250ms';

const shadowLength =
  cardHeight * 0.6;
const shadowStepsPerPixel =
  2;
const shadowSteps =
  shadowLength * shadowStepsPerPixel;

const suit = (foreground, background) => ({
  'color': foreground,
  'background-color': background,
  'text-shadow': range(1, shadowSteps).map((step) => [
    `${step / shadowStepsPerPixel}px`,
    `${step / shadowStepsPerPixel}px`,
    '0',
    (color(background)
      .darken(
        0.2 * (shadowSteps - step - 1) / shadowSteps
      )
      .hexString()
    ),
  ].join(' ')).join(', '),
});

module.exports = {
  '.card': {
    'width': units.px(cardWidth),
    'height': units.px(cardHeight),
    'box-shadow': [
      '0',
      units.px(elevation * 2),
      units.px(elevation * 6),
      utils.blackOpacity(0.15),
    ].join(' '),
    'border-radius': units.px(pxPerInch * 0.1),
    'margin': `0 ${units.px(cardSpacing)}`,
    'flex-shrink': '0',
    'font-size': units.sp(56),
    'line-height': units.px(cardHeight - 4),
    'text-align': 'center',
    'overflow': 'hidden',
    'font-weight': '900',
    'cursor': 'default',
    'display': 'inline-block',
    'animation': [
      `card·margin ${animationDuration} cubic-bezier(0, 0.2, 0, 1)`,
      `card·opacity ${animationDuration} ease-out`,
    ].join(', '),
  },

  '.card·diamonds':
    suit(colors.accent, 'white'),

  '.card·hearts':
    suit('white', colors.accent),

  '.card·clubs':
    suit(colors.primary.dark, 'white'),

  '.card·spades':
    suit('white', colors.primary.dark),

  '.card’s-container': {
    'font-size': '0',
    'line-height': '0',
  },

  '@keyframes card·margin': {
    '0%': {
      'margin-left': units.px(-cardWidth),
    },
    '100%': {
      'margin-left': units.px(cardSpacing),
    },
  },

  '@keyframes card·opacity': {
    '0%': {
      'opacity': '0',
    },
    '100%': {
      'opacity': '1',
    },
  },
};
