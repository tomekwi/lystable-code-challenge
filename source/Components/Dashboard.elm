module Components.Dashboard where

import Html exposing (Html, div, h2, button, text, node)
import Html.Attributes exposing (class, classList, attribute, key)
import Html.Events exposing (onClick)
import Components.Hand as Hand exposing
  ( Sum(BlackjackOrLower, Busted)
  )


-- MODEL

type alias Model =
  { playerSum : Sum
  , gameStatus : GameStatus
  }

type GameStatus
  = Won
  | Draw
  | Lost
  | StillPlaying


-- VIEW

type alias Context =
  { hit : Signal.Address ()
  , stick : Signal.Address ()
  , restart : Signal.Address ()
  }

type ButtonFlag
  = Primary

view : Context -> Model -> Html
view context model =
  let
    actionPanel =
      case model.gameStatus of
        Won ->
          statusDisplay {label = "Victory! You’ve won."}
          |> withResetButton

        Draw ->
          statusDisplay {label = "Fair enough! That’s a draw."}
          |> withResetButton

        Lost ->
          statusDisplay {label = "Blimey! You’ve lost this time."}
          |> withResetButton

        StillPlaying ->
          [ statusDisplay {label = counterMessage}
          , dashboardButton
            { flags = []
            , action = (context.hit, ())
            , icon = "add"
            , nodeKey = "hitButton"
            , label = "Hit"
            }
          , dashboardButton
            { flags = [Primary]
            , action = (context.stick, ())
            , icon = "check"
            , nodeKey = "stickButton"
            , label = "Stick"
            }
          ]

    statusDisplay {label} =
      div
        [ class "dashboard’s-status-bar"
        ]
        [ text label
        ]

    counterMessage =
      case model.playerSum of
        BlackjackOrLower 21 ->
          "Blackjack!"

        BlackjackOrLower number ->
          "You’re at " ++ toString number

        Busted ->
          "Busted!"

    dashboardButton {flags, action, icon, nodeKey, label} =
      node "paper-button"
        [ classList
          [ (,) "dashboard’s-button" True
          , (,) "dashboard’s-button·primary" <| List.member Primary flags
          ]
        , onClick (fst action) (snd action)
        , key nodeKey
        ]
        [ node "iron-icon"
            [ class "dashboard’s-button-icon"
            , attribute "icon" <| "icons:" ++ icon
            ] []
        , text <| " " ++ label
        ]

    withResetButton display =
      display
      :: [ dashboardButton
            { flags = []
            , action = (context.restart, ())
            , icon = "refresh"
            , nodeKey = "restartButton"
            , label = "Play again"
            }
         ]

  in
    div [class "dashboard"]
      actionPanel
