require('style!jss-lite!./styles');  // eslint-disable-line import/no-unresolved

// Add `<meta name=theme-color>`;
const colors = require('./styles/colors');
const themeColor = document.createElement('meta');
themeColor.setAttribute('name', 'theme-color');
themeColor.setAttribute('content', colors.primary.dark);
document.head.appendChild(themeColor);

// Wire things up
const Elm = require('./Main.elm');
const main = document.getElementById('main');
Elm.embed(Elm.Main, main);
