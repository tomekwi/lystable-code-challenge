const reset = require('./styles/reset.style');
const fonts = require('./styles/fonts.style');

const blackjack = require('./Components/Blackjack.style');
const dashboard = require('./Components/Dashboard.style');
const card = require('./Components/Card.style');

module.exports = [
  reset,
  blackjack,
  dashboard,
  card,
].concat(
  fonts
);
