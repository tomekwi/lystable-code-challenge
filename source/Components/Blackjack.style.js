const colors = require('../styles/colors');
const g = require('../styles/global');
const units = require('../styles/units');
const textColors = require('../styles/textColors');

const handPadding =
  30;
const spaceForHandLabel =
  15;

module.exports = {
  '.blackjack': {
    'display': 'flex',
    'flex-direction': 'column',
    'height': '100%',
    'font-family': '"Source Sans Pro", sans-serif',
    'line-height': units.px(g.lineHeight),
    'color': textColors.primary,
    'font-size': units.sp(14),
  },

  '.blackjack’s-hand-background': {
    'position': 'relative',
    'display': 'flex',
    'flex-direction': 'column',
    'justify-content': 'center',
  },

  '.blackjack’s-hand-background·dealer': {
    'background-color': colors.primary.light,
  },

  '.blackjack’s-hand-background·player': {
    'flex-grow': '1',
  },

  '.blackjack’s-hand-label': {
    'position': 'absolute',
    'font-weight': '900',
    'font-size': units.sp(12),
    'text-transform': 'uppercase',
    'letter-spacing': '0.2em',
    'word-spacing': '0.2em',
    'left': '50%',
    'transform': 'translateX(-50%)',
    'color': textColors.hint,
  },

  '.blackjack’s-hand-background·player .blackjack’s-hand-label': {
    'top': '0',
  },

  '.blackjack’s-hand-background·dealer .blackjack’s-hand-label': {
    'bottom': '0',
  },

  '.blackjack’s-hand': {
    'overflow-x': 'auto',
    'border': 'solid transparent',
    'white-space': 'nowrap',
    'text-align': 'center',
  },

  '.blackjack’s-hand-background·dealer .blackjack’s-hand': {
    'padding': [
      units.px(handPadding),
      units.px(handPadding),
      units.px(spaceForHandLabel + handPadding),
    ].join(' '),
  },

  '.blackjack’s-hand-background·player .blackjack’s-hand': {
    'padding': [
      units.px(spaceForHandLabel + handPadding),
      units.px(handPadding),
      units.px(handPadding),
    ].join(' '),
  },
};
